package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.Stack;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private static boolean isCorrect = true; //is false if the original statement is incorrect
    private static final String allowedSymbols = "-+/*().";

    public static String evaluate(String statement) {
        String postfix = "";
        try {
            postfix = infixToPostfix(statement);
        } catch (NullPointerException nullPointerException) {
            isCorrect = false;
        }
        if (postfix.indexOf('(') >= 0 || postfix.indexOf(')') >= 0) { //checking for leftover extra parentheses
            isCorrect = false;
        }
        if (isCorrect) {
            Stack<Double> numbers = new Stack<>();
            Scanner scanner = new Scanner(postfix);
            double result = 0.0;
            //making calculations
            while (scanner.hasNext()) {
                String token = scanner.next();
                try {
                    numbers.push(Double.parseDouble(token));
                } catch (Exception exception) {
                    double num2 = numbers.pop();
                    double num1 = numbers.pop();
                    switch (token) {
                        case "-":
                            result = num1 - num2;
                            break;
                        case "+":
                            result = num1 + num2;
                            break;
                        case "*":
                            result = num1 * num2;
                            break;
                        case "/":
                            try {
                                result = num1 / num2;
                            } catch (ArithmeticException arithmeticException) {
                                isCorrect = false;
                            }
                            break;
                        default:
                            break;
                    }
                    numbers.push(result);
                }
            }
            scanner.close();
            //performing rounding
            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.CEILING);
            return df.format(numbers.pop()).replace(',', '.');
        } else {
            return null;
        }
    }
    //method for transforming the statement into postfix notation
    public static String infixToPostfix(String infix) {
        StringBuilder postfix = new StringBuilder();
        Stack<Character> operators = new Stack<>();

        for (int i = 0; i < infix.length(); i++) {
            char token = infix.charAt(i);
            if (Character.isDigit(token) || token == '.' && infix.charAt(i + 1) != '.') {
                postfix.append(token);
            } else if (allowedSymbols.indexOf(token) < 0 || token != ')'
                    && !Character.isDigit(infix.charAt(i + 1)) && infix.charAt(i + 1) != '(') {
                isCorrect = false;
                break;
                } else {
                    postfix.append(' ');
                    if (operators.isEmpty() || token == '(' ||
                            checkPrecedence(operators.peek()) < checkPrecedence(token)) {
                        operators.push(token);
                    } else if (token == ')') {
                        while (operators.peek() != '(') {
                            postfix.append(' ').append(operators.pop());
                        }
                        operators.pop();
                    } else {
                        while (!operators.isEmpty() && checkPrecedence(operators.peek()) >= checkPrecedence(token)) {
                            postfix.append(' ').append(operators.pop());
                        }
                        operators.push(token);
                        postfix.append(' ');
                    }
                }
        }

        while (!operators.isEmpty()) {
            postfix.append(' ').append(operators.pop());
        }
        return postfix.toString();
    }
    //method for determining the precedence of operators
    public static int checkPrecedence(char operator) {
        switch (operator) {
            case '(':
                return 1;
            case '-':
            case '+':
                return 2;
            case '*':
            case '/':
                return 3;
            default:
                return 0;
        }
    }
}
