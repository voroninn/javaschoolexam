package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")

//    public static void main(String[] args) {
//        System.out.println(find(Stream.of(1, 3, 5, 7, 9).collect(toList()),
//                Stream.of(10, 1, 2, 3, 4, 3, 5, 7, 9, 20).collect(toList())));
//    }

    public static boolean find(List x, List y) throws IllegalArgumentException {
        boolean isSubsequence = true;
        int index = -1;

        if (x != null && y != null) {
            for (Object obj : x) {
                if (y.indexOf(obj) > index) {
                    index = y.indexOf(obj);
                } else if (y.lastIndexOf(obj) > index) {
                    index = y.lastIndexOf(obj);
                } else {
                    isSubsequence = false;
                    break;
                }
            }
        } else {
            throw new IllegalArgumentException();
        }
        return isSubsequence;
    }
}
