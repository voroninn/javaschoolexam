package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int rows = 0;
        int numbersCount = 0;
        //sorting the numbers in ascending order
        try {
            Collections.sort(inputNumbers);
        } catch (Throwable t) {
            throw new CannotBuildPyramidException();
        }
        //determining the number of rows
        while (numbersCount < inputNumbers.size()) {
            numbersCount += ++rows;
        }
        //checking if the numbers can fit into the pyramid
        if (numbersCount > inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }
        //determining the number of columns and initializing the matrix
        int columns = rows + (rows - 1);
        int[][] pyramid = new int[rows][columns];

        int index = 0;
        int counter = 1;
        //filling the matrix with numbers
        for (int i = 0; i < rows; i++) {
            int start = columns / 2 - i; //shifting one step to the left from the center with each cycle
            for (int j = 0; j < counter * 2; j += 2) {
                pyramid[i][start + j] = inputNumbers.get(index++);
            }
            counter++;
        }

        return pyramid;
    }
}
